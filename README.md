# SPA Project

This project is about a hottub with a raspberry pi as the controller.

A control panel has been built to host the electrical devices such as relay driven by the raspberry pi, 
transformator to get power from 120v Ac and convert it to 5vdc, the 240v AC input and all the connections with 
each devices.

This project drive 3 differents pump. A small pump for the circulation, a two speed pump used for cleaning and a very powerful single speed pump used for jet.
There's also a 12vdc led light, an air blower and the water heater to handle.

The API Project contain the core and the rest api project and a swagger interface to handle the rest api.

The client project is a user interface made with React handling the rest api.

For more details about schemas, printed parts, image, videos and other stuff, contact me. This project is in use,  my hottub is running on it since 2019.