from datetime import datetime, timedelta

from core.service_interface import ServiceInterface
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class LightService(ServiceInterface):
    _spa_input: SpaInput
    _spa_output: SpaOutput
    _is_cleaning: bool = False
    _is_in_schedule: bool = True
    _is_light_on: bool = False
    _is_starting: bool = False
    _light_on_since: datetime
    _settings: UserSettings

    def __init__(self, spa_input: SpaInput, spa_output: SpaOutput, settings: UserSettings):
        self._spa_input = spa_input
        self._spa_output = spa_output
        self._settings = settings
        self._light_on_since = datetime.max
        print("LIGHT SERVICE")

    def set_is_starting(self, param):
        self._is_starting = param

    def set_is_light_on(self, param):
        self._is_light_on = param

    def set_is_in_schedule(self, param):
        self._is_in_schedule = param

    def set_is_cleaning(self, param):
        self._is_cleaning = param

    def execute(self):
        # auto shutdown after 2 hours
        if self._is_light_on and datetime.now() - self._light_on_since > timedelta(hours=3):
            self._settings.set_requested_light(False)
            self._is_light_on = False
            self._light_on_since = datetime.now()
            self._spa_output.light_off()
            print("Light off after 2 hours.")
            return

        if self._spa_output.is_light_on() == self._is_light_on:
            # no change
            return

        if self._is_light_on:
            self._spa_output.light_on()
            self._light_on_since = datetime.now()
            print("Light on")
        else:
            self._spa_output.light_off()
            self._light_on_since = datetime.now()
            print("Light off")
