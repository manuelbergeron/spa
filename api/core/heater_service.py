import math

from core.service_interface import ServiceInterface
from spa_gpio.flow_status import FlowStatus
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class HeaterService(ServiceInterface):
    _spa_input: SpaInput
    _spa_output: SpaOutput
    _input_temperature: float = 0
    _output_temperature: float = 0
    _requested_temperature: float = 0
    _temperature_margin: float = 4
    _is_cleaning: bool = False
    _is_in_schedule: bool = True
    _is_starting: bool = False

    def __init__(self, spa_input: SpaInput, spa_output: SpaOutput):

        self._spa_input = spa_input
        self._spa_output = spa_output
        print("TEMPERATURE SERVICE")

    def set_is_starting(self, param):
        self._is_starting = param

    def set_temperature(self, temperature: float):
        if self._requested_temperature != temperature:
            print(f"Changing temperature for: {temperature}")
        self._requested_temperature = temperature

    def get_temperature(self):
        return self._requested_temperature

    def execute(self):
        self.update_temperatures()
        if not self.is_it_possible_to_start_the_heater() and self._spa_output.is_heater_on():
            self._spa_output.stop_heater()
            print("Stopping heater")
            return

        if self._spa_output.is_heater_on() \
                and math.floor(self._input_temperature) > self._requested_temperature:
            self._spa_output.stop_heater()
            print("Stopping heater")
            return

        if not self._spa_output.is_heater_on() \
                and self.is_it_possible_to_start_the_heater() \
                and math.ceil(self._input_temperature) < self._requested_temperature:
            print("Starting heater")
            self._spa_output.start_heater()

    def update_temperatures(self):
        self._input_temperature = self._spa_input.get_input_temperature()
        self._output_temperature = self._spa_input.get_output_temperature()

    def is_it_possible_to_start_the_heater(self) -> bool:
        return self.is_in_schedule() \
               and not self.is_sensor_not_working() \
               and self.is_not_overheating() \
               and self._spa_input.get_flow_status() == FlowStatus.ON \
               and not self._spa_output.is_jet_pump_on() \
               and self._spa_output.is_circulation_pump_on()

    def is_not_overheating(self) -> bool:
        return self._input_temperature <= self._output_temperature + self._temperature_margin

    def is_sensor_not_working(self) -> bool:
        return self._input_temperature == self._output_temperature \
               and self._input_temperature < 1 \
               and self._output_temperature < 1

    def is_in_schedule(self):
        return self._is_in_schedule

    def is_cleaning(self):
        return self._is_cleaning

    def set_is_cleaning(self, status: bool):
        self._is_cleaning = status

    def set_is_in_schedule(self, param):
        self._is_in_schedule = param
