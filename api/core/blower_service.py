from datetime import datetime, timedelta

from core.service_interface import ServiceInterface
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class BlowerService(ServiceInterface):


    _spa_input: SpaInput
    _spa_output: SpaOutput
    _is_cleaning: bool = False
    _is_in_schedule: bool = True
    _is_blower_on: bool = False
    _is_starting: bool = False
    _settings: UserSettings
    _blower_on_since: datetime

    def __init__(self, spa_input: SpaInput, spa_output: SpaOutput, settings: UserSettings):
        self._spa_input = spa_input
        self._spa_output = spa_output
        self._settings = settings
        print("BLOWER SERVICE")

    def set_is_in_schedule(self, param):
        self._is_in_schedule = param

    def set_is_cleaning(self, param):
        self._is_cleaning = param

    def set_is_blower_on(self, param):
        self._is_blower_on = param

    def set_is_starting(self, param):
        self._is_starting = param

    def execute(self):

        if self._spa_output.is_blower_on() and datetime.now() - self._blower_on_since > timedelta(minutes=20):
            self.force_stop_blower()
            print("Stopping blower after 20 minutes.")
            return

        if self._spa_output.is_blower_on() == self._is_blower_on:
            return

        if self._is_blower_on and not self._spa_output.is_blower_on():
            self._spa_output.start_blower()
            self._blower_on_since = datetime.now()
            print("Starting blower.")
            return

        if not self._is_blower_on and self._spa_output.is_blower_on():
            self._spa_output.stop_blower()
            print("Stopping blower.")
            return

    def force_stop_blower(self):
        self._spa_output.stop_blower()
        self._is_blower_on = False
        self._settings.set_requested_blower(False)
