from datetime import datetime, timedelta
from os import path
import csv
from typing import List, Dict

from core.service_interface import ServiceInterface
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class LoggingService(ServiceInterface):
    _spa_input: SpaInput
    _spa_output: SpaOutput
    _is_cleaning: bool = False
    _is_in_schedule: bool = True
    _is_light_on: bool = False
    _is_starting: bool = False
    _light_on_since: datetime
    _settings: UserSettings
    _headers: List = ['date',
                      'input_temperature',
                      'output_temperature',
                      'requested_temperature',
                      'heater_status',
                      'is_circulation_pump_on',
                      'get_requested_circulation_pump',
                      'flow_status',
                      'get_requested_cleaning',
                      'get_requested_cleaning_pump_speed',
                      'get_cleaning_pump_status',
                      'is_jet_pump_on',
                      'get_requested_jet_pump',
                      'blower_status',
                      'requested_blower_status',
                      'light_status',
                      'requested_light_status',
                      'is_cleaning', 'is_in_schedule']

    def __init__(self, spa_input: SpaInput, spa_output: SpaOutput, settings: UserSettings):
        self._spa_input = spa_input
        self._spa_output = spa_output
        self._settings = settings

        print("LOGGING SERVICE")

    def set_is_starting(self, param):
        self._is_starting = param

    def set_is_in_schedule(self, param):
        self._is_in_schedule = param

    def set_is_cleaning(self, param):
        self._is_cleaning = param

    def execute(self):
        if not path.isfile(self.get_file_path()):
            self.create_log_file()

        self.write_log_line()

    def write_log_line(self):
        with open(self.get_file_path(), 'a', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow([
                datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f'),
                self._spa_input.get_input_temperature(),
                self._spa_input.get_output_temperature(),
                self._settings.get_requested_temperature(),
                self._spa_output.is_heater_on(),
                self._spa_output.is_circulation_pump_on(),
                self._settings.get_requested_circulation_pump(),
                self._spa_input.get_flow_status(),
                self._settings.get_requested_cleaning(),
                self._settings.get_requested_cleaning_pump_speed(),
                self._spa_output.get_cleaning_pump_status(),
                self._spa_output.is_jet_pump_on(),
                self._settings.get_requested_jet_pump(),
                self._spa_output.is_blower_on(),
                self._settings.get_requested_blower(),
                self._spa_output.is_light_on(),
                self._settings.get_requested_light(),
                self._settings.get_requested_cleaning(),
                True  # todo schedule
            ])

    def create_log_file(self):
        with open(self.get_file_path(), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(self._headers)

    def get_file_path(self, day_minus: int = 0) -> str:
        file_date = datetime.now() - timedelta(days=day_minus)
        file_path = path.join(path.dirname(path.realpath(__file__)), "..", 'logs',
                              "spa_" + file_date.strftime('%Y-%m-%d') + ".csv")
        return file_path

    def get_log_data(self, day_minus: int = 0):
        if not path.isfile(self.get_file_path()):
            self.create_log_file()

        log_result: List[Dict] = []
        with open(self.get_file_path(day_minus), 'r', newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            first_line: bool = True
            for row in reader:
                if first_line:
                    first_line = False
                    continue

                col_index = 0
                log_dict: Dict = {}
                for header in self._headers:
                    log_dict[header] = row[col_index]
                    col_index += 1

                log_result.append(log_dict)

            return log_result
