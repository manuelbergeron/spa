#!/usr/bin/python3

from core.core import Core

if __name__ == "__main__":
    core: Core = Core()
    core.start()
