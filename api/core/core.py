import sys
import time
import traceback

from core.blower_service import BlowerService
from core.circulation_pump_service import CirculationPumpService
from core.cleaning_service import CleaningService
from core.heater_service import HeaterService
from core.jet_pump_service import JetPumpService
from core.light_service import LightService
from core.logging_service import LoggingService
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class Core(object):
    _settings: UserSettings
    _is_running: bool = False
    _spa_input: SpaInput
    _spa_output: SpaOutput
    _temperature_service: HeaterService
    _cleaning_service: CleaningService
    _light_service: LightService
    _blower_service: BlowerService
    _jet_pump_service: JetPumpService
    _logging_service: LoggingService
    _circulation_pump_service: CirculationPumpService

    def __init__(self):
        self._spa_input = SpaInput()
        self._spa_output = SpaOutput()
        self._settings = UserSettings()
        self._circulation_pump_service = CirculationPumpService(self._spa_input, self._spa_output, self._settings)
        self._light_service = LightService(self._spa_input, self._spa_output, self._settings)
        self._cleaning_service = CleaningService(self._spa_input, self._spa_output, self._settings)
        self._temperature_service = HeaterService(self._spa_input, self._spa_output)
        self._jet_pump_service = JetPumpService(self._spa_input, self._spa_output, self._settings)
        self._blower_service = BlowerService(self._spa_input, self._spa_output, self._settings)
        self._logging_service = LoggingService(self._spa_input, self._spa_output, self._settings)
        print("Core starting.")

    def start(self):
        self._is_running = True
        while self._is_running:
            try:
                self.update_values()
                self._circulation_pump_service.execute()
                self._cleaning_service.execute()
                self._temperature_service.execute()
                self._light_service.execute()
                self._blower_service.execute()
                self._jet_pump_service.execute()
                self._logging_service.execute()
            except Exception as err:
                print("Exception" + str(err))
                traceback.print_exception(*sys.exc_info())
            time.sleep(1)

    def update_values(self):
        self._settings.load()
        # circulation service

        self._circulation_pump_service.set_is_circulation_on(self._settings.get_requested_circulation_pump())
        self._circulation_pump_service.set_is_in_schedule(True)

        # temperature service
        self._temperature_service.set_temperature(self._settings.get_requested_temperature())
        self._temperature_service.set_is_cleaning(self._settings.get_requested_cleaning())
        self._temperature_service.set_is_in_schedule(True)
        self._temperature_service.set_is_starting(self._circulation_pump_service.is_starting())

        # cleaning service
        self._cleaning_service.set_cleaning_pump_speed(self._settings.get_requested_cleaning_pump_speed())
        self._cleaning_service.set_is_in_schedule(True)
        self._cleaning_service.set_is_starting(self._circulation_pump_service.is_starting())

        # light service
        self._light_service.set_is_light_on(self._settings.get_requested_light())
        self._light_service.set_is_cleaning(self._settings.get_requested_cleaning())
        self._light_service.set_is_in_schedule(True)
        self._light_service.set_is_starting(self._circulation_pump_service.is_starting())

        # blower service
        self._blower_service.set_is_blower_on(self._settings.get_requested_blower())
        self._blower_service.set_is_cleaning(self._settings.get_requested_cleaning())
        self._blower_service.set_is_in_schedule(True)
        self._blower_service.set_is_starting(self._circulation_pump_service.is_starting())

        # jet pump service
        self._jet_pump_service.set_is_cleaning(self._settings.get_requested_cleaning())
        self._jet_pump_service.set_is_jet_pump(self._settings.get_requested_jet_pump())
        self._jet_pump_service.set_is_in_schedule(True)
        self._jet_pump_service.set_is_starting(self._circulation_pump_service.is_starting())

