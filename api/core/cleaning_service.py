from core.service_interface import ServiceInterface
from user_settings import UserSettings
from spa_gpio.pump_speed import PumpSpeed
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class CleaningService(ServiceInterface):

    _spa_input: SpaInput
    _spa_output: SpaOutput
    _is_cleaning: bool = False
    _is_in_schedule: bool = True
    _pump_speed: PumpSpeed = PumpSpeed.OFF
    _settings: UserSettings
    _is_starting: bool = False

    def __init__(self, spa_input: SpaInput, spa_output: SpaOutput, settings: UserSettings):
        self._spa_input = spa_input
        self._spa_output = spa_output
        self._settings = settings
        print("CLEANING SERVICE")

    def set_is_starting(self, param):
        self._is_starting = param

    def execute(self):
        if self._is_starting:
            # pump handled by starting process, do nothing.
            return

        if not self._is_in_schedule:
            if self._spa_output.get_cleaning_pump_status() != PumpSpeed.OFF:
                print("Out of schedule, stopping cleaning pump.")
                self._spa_output.set_cleaning_pump(PumpSpeed.OFF)
                return
            # do nothing, out of schedule
            return

        if self._settings.get_requested_cleaning_pump_speed() != self._spa_output.get_cleaning_pump_status():
            print(f"Setting cleaning pump at speed {self._pump_speed}")
            self._spa_output.set_cleaning_pump(self._pump_speed)
            return
        return

    def is_in_schedule(self):
        return self._is_in_schedule

    def is_cleaning(self):
        return self._pump_speed != PumpSpeed.OFF

    def set_is_cleaning(self, param):
        pass

    def set_is_in_schedule(self, param):
        self._is_in_schedule = param

    def set_cleaning_pump_speed(self, pump_speed: PumpSpeed):
        self._pump_speed = pump_speed
