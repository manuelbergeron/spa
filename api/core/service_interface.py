from abc import ABC, abstractmethod


class ServiceInterface(ABC):

    @abstractmethod
    def execute(self):
        pass

    @abstractmethod
    def set_is_in_schedule(self, param):
        pass

    @abstractmethod
    def set_is_cleaning(self, param):
        pass

    @abstractmethod
    def set_is_starting(self, param):
        pass
