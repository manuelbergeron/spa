from datetime import datetime, timedelta

from core.service_interface import ServiceInterface
from spa_gpio.flow_status import FlowStatus
from spa_gpio.pump_speed import PumpSpeed
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class CirculationPumpService(ServiceInterface):
    def set_is_starting(self, param):
        pass

    _spa_input: SpaInput
    _spa_output: SpaOutput
    _is_in_schedule: bool = True
    _is_cleaning: bool = False
    _is_circulation_on: bool = False
    _is_starting: bool = False
    _starting_time: datetime
    _no_flow_cycle_counter: int
    _settings: UserSettings

    def __init__(self, spa_input: SpaInput, spa_output: SpaOutput, settings: UserSettings):
        self._spa_input = spa_input
        self._spa_output = spa_output
        self._settings = settings
        self._no_flow_cycle_counter = 0
        print("CIRCULATION PUMP SERVICE")

    def is_starting(self):
        return self._is_starting

    def set_is_in_schedule(self, param):
        self._is_in_schedule = param

    def set_is_cleaning(self, param):
        _is_cleaning = param

    def set_is_circulation_on(self, param):
        self._is_circulation_on = param

    def execute(self):
        if not self._is_in_schedule or not self._settings.get_requested_circulation_pump():
            print("Stopping circulation pump by request.")
            self._spa_output.circulation_pump_off()
            return

        if self._settings.get_requested_circulation_pump() and not self._spa_output.is_circulation_pump_on():
            print("Starting circulation pump normally")
            self._spa_output.circulation_pump_on()
            return

        if self._spa_input.get_flow_status() == FlowStatus.OFF:
            self._no_flow_cycle_counter += 1
            if self._no_flow_cycle_counter > 10:
                print("Starting until flow with multiple pump")
                self._start_circulation_until_flow()
            return

        # flow is back
        if self._is_starting:
            print("Flow is back")
            self._no_flow_cycle_counter = 0
            self._spa_output.jet_pump_off()
            self._spa_output.set_cleaning_pump(PumpSpeed.OFF)
            self._is_starting = False

        # pump requested, flow is on, nothing  else to do.
        return

    def _start_circulation_until_flow(self):
        if self._spa_output.is_jet_pump_on():
            # the jet pump is the cause of the lack of flow, wait for it to stop
            return

        if not self._is_starting:
            self._starting_time = datetime.now()

        self._is_starting = True
        if self._spa_input.get_flow_status() != FlowStatus.ON:
            time_delta = datetime.now() - self._starting_time

            if not self._spa_output.is_circulation_pump_on():
                print("starting circulation pump")
                self._spa_output.circulation_pump_on()
                self._spa_output.jet_pump_off()
                self._spa_output.set_cleaning_pump(PumpSpeed.OFF)
                return
            if timedelta(seconds=30) <= time_delta < timedelta(seconds=60):
                print("Start boost low")
                self._spa_output.boost_circulation_pump_low()
                return

            if timedelta(seconds=60) <= time_delta < timedelta(seconds=90):
                print("Start boost high")
                self._spa_output.boost_circulation_pump_high()
                return

            if timedelta(seconds=90) <= time_delta < timedelta(seconds=120):
                print("Start boost very high")
                self._spa_output.boost_circulation_pump_very_high()
                return

            if timedelta(seconds=120) <= time_delta < timedelta(seconds=125):
                print("All pump off for 5 seconds")
                self._spa_output.circulation_pump_off()
                self._spa_output.jet_pump_off()
                self._spa_output.set_cleaning_pump(PumpSpeed.OFF)
                return

            if timedelta(seconds=125) <= time_delta:
                print("restart starting sequence")
                self._is_starting = False
                return
