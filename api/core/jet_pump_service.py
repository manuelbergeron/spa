from datetime import datetime, timedelta

from core.service_interface import ServiceInterface
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class JetPumpService(ServiceInterface):
    _spa_input: SpaInput
    _spa_output: SpaOutput
    _is_cleaning: bool = False
    _is_in_schedule: bool = True
    _is_jet_pump_on: bool = False
    _is_starting: bool = False
    _settings: UserSettings
    _jet_pump_on_since: datetime

    def __init__(self, spa_input: SpaInput, spa_output: SpaOutput, settings: UserSettings):
        self._spa_input = spa_input
        self._spa_output = spa_output
        self._settings = settings

        print("JET PUMP SERVICE")

    def set_is_starting(self, param):
        self._is_starting = param

    def set_is_in_schedule(self, param):
        self._is_in_schedule = param

    def set_is_cleaning(self, param):
        self._is_cleaning = param

    def set_is_jet_pump(self, is_jet_pump: bool):
        self._is_jet_pump_on = is_jet_pump

    def execute(self):
        # no change
        if self._is_starting:
            # pump handled by starting process, do nothing.
            return

        if self._settings.get_requested_jet_pump():
            if self._spa_output.is_jet_pump_on():
                # do nothing
                return
            else:
                print("Starting the jet pump")
                self._spa_output.jet_pump_on()
                self._jet_pump_on_since = datetime.now()
        else:
            if not self._spa_output.is_jet_pump_on():
                # do nothing
                return
            else:
                print("Stopping the jet pump")
                self._spa_output.jet_pump_off()

        # stop after 20 minutes
        if self._is_jet_pump_on and self._spa_output.is_jet_pump_on() \
                and datetime.now() - self._jet_pump_on_since > timedelta(minutes=20):
            self.force_stop_jet_pump()
            print("Stopping the jet pump after 20 minutes.")
            return

        # request for a stop
        if not self._settings.get_requested_jet_pump() and self._spa_output.is_jet_pump_on():
            self._spa_output.jet_pump_off()
            print("Stopping the jet pump.")
            return

    def force_stop_jet_pump(self):
        self._is_jet_pump_on = False
        self._settings.set_requested_jet_pump(False)
        self._spa_output.jet_pump_off()
