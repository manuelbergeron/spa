# SPA

A raspberry pi hot tub controller using python as a rest api in backend and Rest as the client with Node.js

# Setup the raspberry PI 

## Raspberry PI Python Setup

Assuming the raspberry Pi is running on Rasbian

Install the dev kit librairies
```
sudo apt-get install build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev python3 python3-dev python3-venv python3-pip libffi-dev libtiff-dev autoconf libopenjp2-7 -y
```

Set the version= to the wanted python version. When I made this project, the latest stable was 3.9.2.
```
version=3.9.2 
```

Install python from tar.xz file.
```
cd /tmp
tar xf Python-$version.tar.xz
cd Python-$version
./configure --enable-optimizations
sudo make altinstall
sudo apt -y autoremove
sudo rm -rf /tmp/Python-$version
rm /tmp/Python-$version.tar.xz
```

Setup python path
````
sudo ln -s /usr/local/bin/python3.9 /usr/bin/python3.9
sudo ln -s /usr/local/bin/python3.9 /usr/bin/python3.9

echo "alias python=/usr/local/bin/python3.9" >> ~/.bashrc

echo "alias python3=/usr/local/bin/python3.9" >> ~/.bashrc

source ~/.bashrc

cd ..

. ~/.bashrc
````

For some unknown reason a library was missing on the raspberry pi to use pipenv. 
Install it with this command:
````
 sudo apt install python-backports.functools-lru-cache
````

Now your raspberry pi should be ready to run the python project.



## Setup your python development environment
This project use pipenv virtual environment. 

Install pipenv through pip
```
pip install pipenv
```

Shell into the virtual environment
````
cd ./api
pipenv shell
````

# Setup the services

Followed this procedure 
https://www.e-tinkers.com/2018/08/how-to-properly-host-flask-application-with-nginx-and-guincorn/


If change are made to the rest api
````
sudo systemctl daemon-reload
sudo systemctl restart gunicorn
````

# Nginx configuration

````bashrc
server {
        listen 80 default_server;
        listen [::]:80;

        root /home/pi/projects/spa/client/build;

        # Try static file first then php
        index index.html index.htm index.php;

        error_log /var/log/nginx/spa-error.log;

        server_name spa;


        location / {
            try_files $uri $uri/index.html @wsgi;
            index index.html;
        }

        location @wsgi {
            proxy_pass http://unix:/tmp/gunicorn.sock;
            include proxy_params;
            add_header 'Access-Control-Allow-Origin' '*';
        }

        location ~* .(svgz|eot|otf|woff|mp4|ttf|rss|atom|zip|tgz|gz|rar|bz2|doc|xls|exe|ppt|tar|mid|midi|wav)$ {
            access_log off;
            log_not_found off;
            expires max;
        }
}
````