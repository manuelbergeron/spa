from enum import Enum


class PumpSpeed(Enum):
    OFF = 0
    LOW = 1
    HIGH = 2
