import os
from datetime import datetime, timedelta

from .flow_status import FlowStatus
from .thermistor import convert_volt_to_celsius
from gpiozero import MCP3008, Button, Device
from gpiozero.pins.mock import MockFactory


class SpaInput(object):
    FLOW_VALVE_PIN = 26  # 37
    INPUT_TEMPERATURE_CHANNEL = 0
    OUTPUT_TEMPERATURE_CHANNEL = 1

    _input_temperature_sensor: MCP3008
    _output_temperature_sensor: MCP3008
    _flow_valve: Button
    _last_temperature_request: datetime = datetime.min
    _last_input_temperature: float = 0
    _last_output_temperature: float = 0

    def __init__(self):
        if os.getenv('GPIOZERO_PIN_FACTORY') == "mock python3":
            Device.pin_factory = MockFactory()

        self._input_temperature_sensor = MCP3008(channel=self.INPUT_TEMPERATURE_CHANNEL)
        self._output_temperature_sensor = MCP3008(channel=self.OUTPUT_TEMPERATURE_CHANNEL)
        self._flow_valve = Button(self.FLOW_VALVE_PIN)

    def get_input_temperature(self) -> float:
        if datetime.now() - self._last_temperature_request > timedelta(seconds=1):
            volt_input = self._input_temperature_sensor.value
            self._last_input_temperature = convert_volt_to_celsius(volt_input)

        return self._last_input_temperature

    def get_output_temperature(self) -> float:
        if datetime.now() - self._last_temperature_request > timedelta(seconds=1):
            volt_output = self._output_temperature_sensor.value
            self._last_output_temperature = convert_volt_to_celsius(volt_output)

        return self._last_output_temperature

    def get_flow_status(self) -> FlowStatus:
        return FlowStatus.ON if self._flow_valve.value == 1 else FlowStatus.OFF
