from spa_gpio.spa_output import SpaOutput
from spa_gpio.spa_input import SpaInput
from spa_gpio.pump_speed import PumpSpeed
import time

if __name__ == "__main__":
    spa_output = SpaOutput()
    spa_input = SpaInput()

    for i in range(10):
        spa_output.light_on()
        time.sleep(2)
        spa_output.light_off()
        time.sleep(2)
        spa_output.circulation_pump_on()
        time.sleep(2)
        spa_output.circulation_pump_off()
        time.sleep(2)
        spa_output.set_cleaning_pump(PumpSpeed.LOW)
        time.sleep(2)
        spa_output.set_cleaning_pump(PumpSpeed.HIGH)
        time.sleep(2)
        spa_output.set_cleaning_pump(PumpSpeed.OFF)
        time.sleep(2)
        spa_output.jet_pump_on()
        time.sleep(2)
        spa_output.jet_pump_off()
        time.sleep(2)
        spa_output.start_heater()
        time.sleep(2)
        spa_output.stop_heater()
        time.sleep(2)
        spa_output.start_blower()
        time.sleep(2)
        spa_output.stop_blower()
        time.sleep(2)
        print("Three stage of circulation pump visited.")
