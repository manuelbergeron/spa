import os
from gpiozero import OutputDevice, Device
from gpiozero.pins.mock import MockFactory
from spa_gpio.pump_speed import PumpSpeed


class SpaOutput(object):
    CIRCULATION_PUMP = 2  # 3
    CLEANING_PUMP_LOW = 4  # 7
    CLEANING_PUMP_HIGH = 3  # 5
    JET_PUMP = 27  # 11
    HEATER = 17  # 13
    BLOWER = 22  # 15
    LIGHT = 5  # 29

    _circulation_pump: OutputDevice = None
    _cleaning_pump_low: OutputDevice = None
    _cleaning_pump_high: OutputDevice = None
    _jet_pump: OutputDevice = None
    _heater: OutputDevice = None
    _blower: OutputDevice = None
    _light: OutputDevice = None

    def __init__(self):
        if os.getenv('GPIOZERO_PIN_FACTORY') == "mock python3":
            Device.pin_factory = MockFactory()

        self.setup_gpio()

    def setup_gpio(self):
        self._circulation_pump = OutputDevice(self.CIRCULATION_PUMP, active_high=False, initial_value=False)
        self._cleaning_pump_low = OutputDevice(self.CLEANING_PUMP_LOW, active_high=False, initial_value=False)
        self._cleaning_pump_high = OutputDevice(self.CLEANING_PUMP_HIGH, active_high=False, initial_value=False)
        self._jet_pump = OutputDevice(self.JET_PUMP, active_high=False, initial_value=False)
        self._heater = OutputDevice(self.HEATER, active_high=False, initial_value=False)
        self._blower = OutputDevice(self.BLOWER, active_high=False, initial_value=False)
        self._light = OutputDevice(self.LIGHT, active_high=True, initial_value=False)

    def jet_pump_on(self):
        self._jet_pump.on()

    def jet_pump_off(self):
        self._jet_pump.off()

    def circulation_pump_on(self):
        self._circulation_pump.on()

    def circulation_pump_off(self):
        self._circulation_pump.off()

    def set_cleaning_pump(self, speed: PumpSpeed):
        if speed == PumpSpeed.OFF:
            self._cleaning_pump_high.off()
            self._cleaning_pump_low.off()
        elif speed == PumpSpeed.LOW:
            self._cleaning_pump_high.off()
            self._cleaning_pump_low.on()
        else:
            self._cleaning_pump_low.off()
            self._cleaning_pump_high.on()

    def light_on(self):
        self._light.on()

    def light_off(self):
        self._light.off()

    def is_light_on(self):
        return self._light.is_active

    def start_blower(self):
        self._blower.on()

    def stop_blower(self):
        self._blower.off()

    def start_heater(self):
        self._heater.on()

    def stop_heater(self):
        self._heater.off()

    def boost_circulation_pump_low(self):
        self.circulation_pump_on()
        self.set_cleaning_pump(PumpSpeed.LOW)
        self.jet_pump_off()

    def boost_circulation_pump_high(self):
        self.circulation_pump_on()
        self.set_cleaning_pump(PumpSpeed.HIGH)
        self.jet_pump_off()

    def boost_circulation_pump_very_high(self):
        self.circulation_pump_on()
        self.set_cleaning_pump(PumpSpeed.HIGH)
        self.jet_pump_on()

    def is_jet_pump_on(self) -> bool:
        return self._jet_pump.is_active

    def get_cleaning_pump_status(self) -> PumpSpeed:
        if self._cleaning_pump_high.is_active:
            return PumpSpeed.HIGH

        if self._cleaning_pump_low.is_active:
            return PumpSpeed.LOW

        return PumpSpeed.OFF

    def is_circulation_pump_on(self) -> bool:
        return self._circulation_pump.is_active

    def is_blower_on(self) -> bool:
        return self._blower.is_active

    def is_heater_on(self) -> bool:
        return self._heater.is_active
