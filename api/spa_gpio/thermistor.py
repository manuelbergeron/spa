def convert_volt_to_celsius(volt: float) -> float:
    if volt < 0.01:
        print("Sensor not working.")
        return volt
    elif 0.01 <= volt < 0.25:
        return round(40 - (200*(volt - 0.25)), 1)
    elif volt >= 0.25:
        return round(40-(100*(volt-0.25)), 1)

