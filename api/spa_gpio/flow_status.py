from enum import Enum


class FlowStatus(Enum):
    OFF = 0
    ON = 1
