from core.core import Core


def main():
    core: Core = Core()
    core.start()


if __name__ == "__main__":
    main()
