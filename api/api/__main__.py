#!/usr/bin/python3

from api.spa_api import SpaApi

if __name__ == "__main__":
    app = SpaApi()
    app.run(host='0.0.0.0')
