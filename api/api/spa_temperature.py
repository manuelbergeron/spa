from api.schemas.temperature_schema import TemperatureSchema
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput
from flask_restful import Resource
from flask_apispec.views import MethodResource
from flask_apispec import marshal_with, doc, use_kwargs


class SpaTemperature(Resource, MethodResource):
    _input: SpaInput
    _output: SpaOutput
    _settings: UserSettings

    def __init__(self, **kwargs):
        self._output = kwargs['outputs']
        self._input = kwargs['inputs']
        self._settings = kwargs['settings']

    @doc(description='Set the spa temperature', tags=['Temperature'])
    @use_kwargs(TemperatureSchema)
    @marshal_with(TemperatureSchema, code=201)  # marshalling
    def post(self, **temperature_schema):
        self._settings.load()
        self._settings.set_requested_temperature(temperature_schema["temperature"])

        return temperature_schema
