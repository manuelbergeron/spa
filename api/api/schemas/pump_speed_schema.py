from marshmallow import Schema, fields


class PumpSpeedSchema(Schema):
    class Meta:
        fields = ('pump_speed',)

    pump_speed = fields.Int(default=0, required=True)
