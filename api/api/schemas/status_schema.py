from marshmallow import Schema, fields

from user_settings import UserSettings
from spa_gpio.flow_status import FlowStatus
from spa_gpio.pump_speed import PumpSpeed
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class StatusSchema(Schema):
    class Meta:
        fields = ('cleaning_pump_speed',
                  'flow_status',
                  'requested_cleaning_pump_speed',
                  'requested_circulation_pump_status',
                  'circulation_pump_status',
                  'requested_jet_pump_status',
                  'jet_pump_status',
                  'requested_blower_status',
                  'blower_status',
                  'requested_light_status',
                  'light_status',
                  'requested_temperature',
                  'input_temperature',
                  'output_temperature',
                  'heater_status',
                  'is_cleaning',
                  'is_in_schedule')

    cleaning_pump_speed = fields.Str(default=PumpSpeed.OFF)
    requested_cleaning_pump_speed = fields.Str(default=PumpSpeed.OFF)
    flow_status = fields.Boolean(default=False)

    requested_circulation_pump_status = fields.Boolean(default=False)

    circulation_pump_status = fields.Boolean(default=False)

    requested_jet_pump_status = fields.Boolean(default=False)

    jet_pump_status = fields.Boolean(default=False)

    requested_blower_status = fields.Boolean(default=False)
    blower_status = fields.Boolean(default=False)

    requested_light_status = fields.Boolean(default=False)

    light_status = fields.Boolean(default=False)

    requested_temperature = fields.Float(default=-1)

    input_temperature = fields.Float(default=-1)

    output_temperature = fields.Float(default=-1)

    heater_status = fields.Boolean(default=False)

    is_cleaning = fields.Boolean(default=False)
    is_in_schedule = fields.Boolean(default=True)

    def update_schema(self, spa_input: SpaInput, spa_output: SpaOutput, settings: UserSettings):
        settings.load()
        self.cleaning_pump_speed = spa_output.get_cleaning_pump_status()
        self.requested_cleaning_pump_speed = settings.get_requested_cleaning_pump_speed()
        self.flow_status = spa_input.get_flow_status() == FlowStatus.ON
        self.requested_circulation_pump_status = settings.get_requested_circulation_pump()
        self.circulation_pump_status = spa_output.is_circulation_pump_on()
        self.requested_jet_pump_status = settings.get_requested_jet_pump()
        self.jet_pump_status = spa_output.is_jet_pump_on()

        self.requested_blower_status = settings.get_requested_blower()
        self.blower_status = spa_output.is_blower_on()

        self.requested_light_status = settings.get_requested_light()
        self.light_status = spa_output.is_light_on()

        self.requested_temperature = settings.get_requested_temperature()
        self.input_temperature = spa_input.get_input_temperature()
        self.output_temperature = spa_input.get_output_temperature()
        self.heater_status = spa_output.is_heater_on()
        self.is_cleaning = settings.get_requested_cleaning()
        self.is_in_schedule = True  # @todo ajouter la schedule
