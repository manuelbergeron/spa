from marshmallow import Schema, fields


class TemperatureSchema(Schema):
    class Meta:
        fields = ('temperature',)

    temperature = fields.Float(default=-1, required=True)
