from marshmallow import Schema, fields


class OnOffShema(Schema):
    class Meta:
        fields = ('status',)

    status = fields.Boolean(default=False, required=True)
