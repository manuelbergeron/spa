from flask import Flask
from flask_restful import Api
from apispec import APISpec
from apispec.ext.marshmallow import MarshmallowPlugin
from flask_apispec.extension import FlaskApiSpec

from api.spa_blower import SpaBlower
from api.spa_circulation_pump import SpaCirculationPump
from api.spa_cleaning import SpaCleaning
from api.spa_jet_pump import SpaJetPump
from api.spa_light import SpaLight
from api.spa_status import SpaStatus
from api.spa_temperature import SpaTemperature
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class SpaApi(object):
    _api: Api
    _docs: FlaskApiSpec
    _app: Flask

    _spa_input: SpaInput
    _spa_output: SpaOutput
    _settings: UserSettings

    def __init__(self):
        self._spa_input = SpaInput()
        self._spa_output = SpaOutput()
        self._app = Flask("SPA")
        self._api = Api(self._app)
        self._settings = UserSettings()

        self._app.config.update({
            'APISPEC_SPEC': APISpec(
                title='Spa/Hot Tub Project',
                version='v1',
                plugins=[MarshmallowPlugin()],
                openapi_version='2.0.0'
            ),
            'APISPEC_SWAGGER_URL': '/swagger/',  # URI to access API Doc JSON
            'APISPEC_SWAGGER_UI_URL': '/swagger-ui/'  # URI to access UI of API Doc
        })
        self._docs = FlaskApiSpec(self._app)
        self.setup_routes()

    def run(self, host):
        self._app.run(debug=False, host=host)
        return self._app

    def app(self) -> Flask:
        return self._app

    def setup_routes(self):
        self._api.add_resource(SpaStatus, '/status', resource_class_kwargs=self.get_resource_class_kwargs())
        self._docs.register(SpaStatus, resource_class_kwargs=self.get_resource_class_kwargs())

        self._api.add_resource(SpaTemperature, '/temperature', resource_class_kwargs=self.get_resource_class_kwargs())
        self._docs.register(SpaTemperature, resource_class_kwargs=self.get_resource_class_kwargs())

        self._api.add_resource(SpaJetPump, '/jet_pump', resource_class_kwargs=self.get_resource_class_kwargs())
        self._docs.register(SpaJetPump, resource_class_kwargs=self.get_resource_class_kwargs())

        self._api.add_resource(SpaCirculationPump, '/circulation_pump', resource_class_kwargs=self.get_resource_class_kwargs())
        self._docs.register(SpaCirculationPump, resource_class_kwargs=self.get_resource_class_kwargs())

        self._api.add_resource(SpaLight, '/light', resource_class_kwargs=self.get_resource_class_kwargs())
        self._docs.register(SpaLight, resource_class_kwargs=self.get_resource_class_kwargs())

        self._api.add_resource(SpaBlower, '/blower', resource_class_kwargs=self.get_resource_class_kwargs())
        self._docs.register(SpaBlower, resource_class_kwargs=self.get_resource_class_kwargs())

        self._api.add_resource(SpaCleaning, '/cleaning', resource_class_kwargs=self.get_resource_class_kwargs())
        self._docs.register(SpaCleaning, resource_class_kwargs=self.get_resource_class_kwargs())

    def get_resource_class_kwargs(self) -> dict:
        return {'outputs': self._spa_output, 'inputs': self._spa_input, 'settings': self._settings, }
