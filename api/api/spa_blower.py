from api.schemas.on_off_schema import OnOffShema
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput
from flask_restful import Resource
from flask_apispec.views import MethodResource
from flask_apispec import marshal_with, doc, use_kwargs


class SpaBlower(Resource, MethodResource):
    _input: SpaInput
    _output: SpaOutput
    _settings: UserSettings

    def __init__(self, **kwargs):
        self._output = kwargs['outputs']
        self._input = kwargs['inputs']
        self._settings = kwargs['settings']

    @doc(description='Manage the blower', tags=['Blower'])
    @use_kwargs(OnOffShema)
    @marshal_with(OnOffShema, code=201)  # marshalling
    def post(self, **on_off_schema):
        self._settings.load()
        self._settings.set_requested_blower(on_off_schema["status"])

        return on_off_schema
