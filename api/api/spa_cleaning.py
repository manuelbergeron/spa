from api.schemas.pump_speed_schema import PumpSpeedSchema
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput
from flask_restful import Resource
from flask_apispec.views import MethodResource
from flask_apispec import marshal_with, doc, use_kwargs


class SpaCleaning(Resource, MethodResource):
    _input: SpaInput
    _output: SpaOutput
    _settings: UserSettings

    def __init__(self, **kwargs):
        self._output = kwargs['outputs']
        self._input = kwargs['inputs']
        self._settings = kwargs['settings']

    @doc(description='Manage the cleaning', tags=['Cleaning'])
    @use_kwargs(PumpSpeedSchema)
    @marshal_with(PumpSpeedSchema, code=201)  # marshalling
    def post(self, **pump_speed_schema):
        self._settings.load()
        self._settings.set_requested_cleaning_pump_speed(pump_speed_schema["pump_speed"])

        return pump_speed_schema
