from flask_restful import Resource
from flask_apispec.views import MethodResource
from flask_apispec import marshal_with, doc
from api.schemas.status_schema import StatusSchema
from user_settings import UserSettings
from spa_gpio.spa_input import SpaInput
from spa_gpio.spa_output import SpaOutput


class SpaStatus(Resource, MethodResource):
    _input: SpaInput
    _output: SpaOutput
    _settings: UserSettings

    def __init__(self, **kwargs):
        self._output = kwargs['outputs']
        self._input = kwargs['inputs']
        self._settings = kwargs['settings']

    @doc(description='Get the whole spa status.', tags=['Status'])
    @marshal_with(StatusSchema)  # marshalling
    def get(self):
        """
        Get method represents a GET API method
        """
        return_schema = StatusSchema()
        return_schema.update_schema(self._input, self._output, self._settings, )
        return return_schema
