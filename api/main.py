#!/usr/bin/python3

from api.spa_api import SpaApi


def main():
    spa_api = SpaApi()
    spa_api.run(host='0.0.0.0')


if __name__ == "__main__":
    main()
