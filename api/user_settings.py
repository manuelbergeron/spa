import json
from os import path
from spa_gpio.pump_speed import PumpSpeed


class UserSettings(object):
    settings = {
        "requested_temperature": 0,
        "requested_cleaning_pump_speed": 0,
        "requested_jet_pump": False,
        "requested_circulation_pump": False,
        "requested_blower": False,
        "requested_light": False,
    }

    def get_file_path(self) -> str:
        file_path = path.join(path.dirname(path.realpath(__file__)), "settings.json")
        return file_path

    def serialize(self):
        # Serializing json
        json_object = json.dumps(self.settings, indent=4)

        # Writing to sample.json
        with open(self.get_file_path(), "w") as outfile:
            outfile.write(json_object)

    def load(self):
        if not path.exists(self.get_file_path()):
            self.serialize()

        with open(self.get_file_path(), 'r') as openfile:
            # Reading from json file
            self.settings = json.load(openfile)

    def get_requested_temperature(self) -> float:
        return self.settings["requested_temperature"]

    def set_requested_temperature(self, temperature: float) -> None:
        self.settings["requested_temperature"] = temperature
        self.serialize()

    def get_requested_cleaning_pump_speed(self) -> PumpSpeed:

        return PumpSpeed(self.settings["requested_cleaning_pump_speed"])

    def set_requested_cleaning_pump_speed(self, cleaning_pump_speed: int) -> None:
        self.settings["requested_cleaning_pump_speed"] = cleaning_pump_speed
        self.serialize()

    def get_requested_circulation_pump(self) -> bool:
        return bool(self.settings["requested_circulation_pump"])

    def set_requested_circulation_pump(self, is_circulation_pump_on: bool) -> None:
        self.settings["requested_circulation_pump"] = is_circulation_pump_on
        self.serialize()

    def get_requested_jet_pump(self) -> bool:
        return bool(self.settings["requested_jet_pump"])

    def set_requested_jet_pump(self, is_jet_pump_on: bool) -> None:
        self.settings["requested_jet_pump"] = is_jet_pump_on
        self.serialize()

    def get_requested_blower(self) -> bool:
        return bool(self.settings["requested_blower"])

    def set_requested_blower(self, is_blower_on: bool) -> None:
        self.settings["requested_blower"] = is_blower_on
        self.serialize()

    def get_requested_light(self) -> bool:
        return bool(self.settings["requested_light"])

    def set_requested_light(self, is_light_on: bool) -> None:
        self.settings["requested_light"] = is_light_on
        self.serialize()

    def get_requested_cleaning(self) -> bool:
        return PumpSpeed(self.settings["requested_cleaning_pump_speed"]) != PumpSpeed.OFF
