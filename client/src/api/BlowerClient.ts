export class BlowerClient {
    private _serverAddress: string;

    constructor(serverAddress: string) {
        this._serverAddress = serverAddress;
    }

    public sendRequestedStatus(blowerStatus: boolean) {
        this.sendRequestedBlowerStatusAsync(blowerStatus).catch((error) => {
            console.log("error sending the blower status: ");
            console.log(error);
        });
    }

    private async sendRequestedBlowerStatusAsync(blowerStatus: boolean) {
        try {
            await fetch(this._serverAddress + "/blower", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({"status": blowerStatus}),
            });
        }
        catch (error) {
            console.error('An error occurred', error);
            // Handle error here
        }
    }

}

export default BlowerClient;