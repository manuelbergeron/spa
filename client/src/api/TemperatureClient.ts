export class TemperatureClient {
    private _serverAddress: string;

    constructor(serverAddress: string) {
        this._serverAddress = serverAddress;
    }

    public sendTemperature(temperature: number | undefined) {
        this.sendTemperatureAsync(temperature === undefined ? -9999 : temperature).catch((error) => {
            console.log("error sending the temperature: ");
            console.log(error);
        });
    }

    private async sendTemperatureAsync(temperature: number) {
        try {
            await fetch(this._serverAddress + "/temperature", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({"temperature": temperature}),
            });
        }
        catch (error) {
            console.error('An error occurred', error);
            // Handle error here
        }
    }

}

export default TemperatureClient;