export class CirculationPumpClient {
    private _serverAddress: string;

    constructor(serverAddress: string) {
        this._serverAddress = serverAddress;
    }

    public sendRequestedStatus(pumpStatus: boolean) {
        this.sendRequestedCirculationPumpStatusAsync(pumpStatus).catch((error) => {
            console.log("error sending the circulation pump status: ");
            console.log(error);
        });
    }

    private async sendRequestedCirculationPumpStatusAsync(pumpStatus: boolean) {
        try {
            await fetch(this._serverAddress + "/circulation_pump", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({"status": pumpStatus}),
            });
        }
        catch (error) {
            console.error('An error occurred', error);
            // Handle error here
        }
    }

}

export default CirculationPumpClient;