export const enum PumpSpeed {
    OFF = 0,
    LOW = 1,
    HIGH = 2
}