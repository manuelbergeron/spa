import {PumpSpeed} from "./PumpSpeed.ts";

export class CleaningPumpClient {
    private _serverAddress: string;

    constructor(serverAddress: string) {
        this._serverAddress = serverAddress;
    }

    public sendRequestedStatus(pumpSpeed: PumpSpeed) {

        this.sendRequestedCleaningPumpSpeedStatusAsync(pumpSpeed).catch((error) => {
            console.log("error sending the cleaning pump status: ");
            console.log(error);
        });
    }

    private async sendRequestedCleaningPumpSpeedStatusAsync(pumpSpeed: PumpSpeed) {
        console.log(pumpSpeed);
        try {
            await fetch(this._serverAddress + "/cleaning", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({"pump_speed": pumpSpeed === null ? 0 : pumpSpeed}),
            });
        }
        catch (error) {
            console.error('An error occurred', error);
            // Handle error here
        }
    }

}

export default CleaningPumpClient;