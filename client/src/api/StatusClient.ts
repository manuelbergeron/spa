import {useContext, useEffect} from "react";
import StatusContext from "../contexts/StatusContext.tsx";
import {Status} from "./Status.ts";
import {API_URL} from "../constant.ts";

export const useSystemStatusClient = () => {
    const { dispatch } = useContext(StatusContext);

    useEffect(() => {

        const onStatusUpdated = (status : Status) => {
            dispatch({type:"SET_STATUS_FROM_API", payload: status});
        };

        const statusClient: StatusClient = new StatusClient(API_URL, onStatusUpdated);
        statusClient.start();

        return () => {
            statusClient.stop();
        };
    }, [dispatch]);
};


export class StatusClient {
    private _onStatusUpdated: (status: Status) => void;
    private _serverAddress: string;
    private _interval: any;

    constructor(serverAddress: string, onStatusUpdated: (status: Status) => void) {
        this._onStatusUpdated = onStatusUpdated;
        this._serverAddress = serverAddress + "/status";
    }

    public start() {
        this._interval = setInterval(() => this.fetchStatus(), 1000);
    }

    public fetchStatus() {
        fetch(this._serverAddress)
            .then(response => response.json())
            .then((status: Status) => {
                this._onStatusUpdated(status);
            })
            .catch(error => {
                console.error(error);
            });
    }

    public stop() {
        if (this._interval) {
            window.clearInterval(this._interval);
            this._interval = undefined;
        }
    }


}