export class JetPumpClient {
    private _serverAddress: string;

    constructor(serverAddress: string) {
        this._serverAddress = serverAddress;
    }

    public sendRequestedStatus(pumpStatus: boolean) {
        this.sendRequestedJetPumpClientStatusAsync(pumpStatus).catch((error) => {
            console.log("error sending the jet pump status: ");
            console.log(error);
        });
    }

    private async sendRequestedJetPumpClientStatusAsync(pumpStatus: boolean) {
        try {
            await fetch(this._serverAddress + "/jet_pump", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({"status": pumpStatus}),
            });
        }
        catch (error) {
            console.error('An error occurred', error);
            // Handle error here
        }
    }

}

export default JetPumpClient;