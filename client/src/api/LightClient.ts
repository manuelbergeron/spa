export class LightClient {
    private _serverAddress: string;

    constructor(serverAddress: string) {
        this._serverAddress = serverAddress;
    }

    public sendRequestedStatus(lightStatus: boolean) {
        this.sendRequestedLightStatusAsync(lightStatus).catch((error) => {
            console.log("error sending the light status: ");
            console.log(error);
        });
    }

    private async sendRequestedLightStatusAsync(lightStatus: boolean) {
        try {
            await fetch(this._serverAddress + "/light", {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({"status": lightStatus}),
            });
        }
        catch (error) {
            console.error('An error occurred', error);
            // Handle error here
        }
    }

}

export default LightClient;