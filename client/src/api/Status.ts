import {PumpSpeed} from "./PumpSpeed.ts";

export interface Status {
    "blower_status": boolean,
    "circulation_pump_status":boolean,
    "cleaning_pump_speed": PumpSpeed,
    "flow_status":boolean,
    "heater_status":boolean,
    "input_temperature":number,
    "is_cleaning":boolean,
    "is_in_schedule":boolean,
    "jet_pump_status":boolean,
    "light_status":boolean,
    "output_temperature":number,
    "requested_blower_status":boolean,
    "requested_circulation_pump_status":boolean,
    "requested_cleaning_pump_speed": PumpSpeed,
    "requested_jet_pump_status":boolean,
    "requested_light_status":boolean,
    "requested_temperature":number
}

