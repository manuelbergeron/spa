import React, {useEffect, useState} from "react";
import {Chip, FormControlLabel, Grid} from "@mui/material";
import IOSSwitch from "./IOSSwitch.tsx";
import BoltIcon from '@mui/icons-material/Bolt';
import OfflineBoltIcon from '@mui/icons-material/OfflineBolt';

interface OnOffComponentProps {
    label: string,
    value: boolean,
    requestedValue: boolean,
    onValueChange: (newValue: boolean) => void
}

const OnOffComponent : React.FC<OnOffComponentProps> = (props: OnOffComponentProps) => {

    const [value, setValue] = useState(false);
    const [requestedValue, setRequestedValue] = useState(false);

    useEffect(() => {
        setValue(props.value);
        setRequestedValue(props.requestedValue);

    }, [props]);

    return (
        <>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <FormControlLabel
                        control={
                            <IOSSwitch
                                checked={requestedValue}
                                onChange={(_event, checked) => {
                                    setRequestedValue(checked);
                                    props.onValueChange(checked);
                                }}
                                sx={{
                                    mr:1, ml: 1
                                }}
                            />
                        }
                        label={props.label}
                    />
                    {value ?
                        <Chip
                            sx={{ml: 1, float: 'right'}}

                            label="On"
                            color={"info"}
                            icon={<BoltIcon />}
                            size={"medium"}
                        />
                        :
                        <Chip
                            sx={{ml: 1, float: 'right'}}
                            label="Off"
                            color={"default"}
                            icon={<OfflineBoltIcon />}
                            size={"medium"}
                        />
                    }
                </Grid>
            </Grid>
        </>
    );
}

export default OnOffComponent;