import React, {useEffect, useState} from "react";
import {Chip, Grid, ToggleButton, ToggleButtonGroup} from "@mui/material";
import BoltIcon from '@mui/icons-material/Bolt';
import OfflineBoltIcon from '@mui/icons-material/OfflineBolt';
import {PumpSpeed} from "../api/PumpSpeed.ts";

interface PumpSpeedComponentProps {
    label: string,
    value: PumpSpeed,
    requestedValue: PumpSpeed,
    onValueChange: (newValue: number) => void
}

const PumpSpeedComponent : React.FC<PumpSpeedComponentProps> = (props: PumpSpeedComponentProps) => {

    const [value, setValue] = useState<number>(0);
    const [requestedValue, setRequestedValue] = useState<number>(0);

    useEffect(() => {
        switch (props.value.toString()) {
            case"PumpSpeed.HIGH":
                setValue(2);
                break;
            case "PumpSpeed.LOW":
                setValue(1);
                break;
            default:
                setValue(0);
                break;
        }

        switch (props.requestedValue.toString()) {
            case "PumpSpeed.HIGH":
                setRequestedValue(2);
                break;
            case "PumpSpeed.LOW":
                setRequestedValue(1);
                break;
            default:
                setRequestedValue(0);
                break;
        }
    }, [props]);

    return (
        <>
            <Grid container spacing={1}>
                <Grid item xs={12}>
                    {props.label}
                    {value !== 0 ?
                        <Chip
                            sx={{ml: 1, float: 'right'}}
                            label={value === 1 ? "Low" : "High"}
                            color={"info"}
                            icon={<BoltIcon />}
                            size={"medium"}
                        />
                        :
                        <Chip
                            sx={{ml: 1, float: 'right'}}
                            label="Off"
                            color={"default"}
                            icon={<OfflineBoltIcon />}
                            size={"medium"}
                        />
                    }
                </Grid>
                <Grid item xs={12}>
                    <ToggleButtonGroup
                        fullWidth
                        color={"primary"}
                        size={"small"}
                        value={requestedValue}
                        exclusive
                        onChange={(_event, newValue: number) => {
                            console.log(newValue);
                           // setRequestedValue(newValue);
                            props.onValueChange(newValue);
                        }}
                    >
                        <ToggleButton value={0}>
                            Off
                        </ToggleButton>
                        <ToggleButton value={1}>
                            Low
                        </ToggleButton>
                        <ToggleButton value={2}>
                            High
                        </ToggleButton>
                    </ToggleButtonGroup>
                </Grid>
            </Grid>
        </>
    );
}

export default PumpSpeedComponent;