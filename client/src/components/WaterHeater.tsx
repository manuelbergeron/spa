import React, {useEffect, useState} from "react";
import {
    Chip,
    Divider,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    ListSubheader, Slider,

} from "@mui/material";
import ThermostatIcon from '@mui/icons-material/Thermostat';
import LocalFireDepartmentIcon from '@mui/icons-material/LocalFireDepartment';
import {Status} from "../api/Status.ts";
import TemperatureClient from "../api/TemperatureClient.ts";
import {API_URL} from "../constant.ts";
import BoltIcon from "@mui/icons-material/Bolt";
import OfflineBoltIcon from "@mui/icons-material/OfflineBolt";

const marks = [
    {
        value: 0,
        label: ' 0°C',
    },
    {
        value: 10,
        label: '10°C',
    },
    {
        value: 20,
        label: '20°C',
    },
    {
        value: 30,
        label: '30°C',
    },
    {
        value: 40,
        label: '40°C',
    },
    {
        value: 50,
        label: '50°C',
    }
];

interface WaterHeaterProps {
    status: Status
}

const WaterHeater : React.FC<WaterHeaterProps> = (props: WaterHeaterProps) => {
    const [status, setStatus] = useState<Status>(props.status);
    const [isSliding, setIsSliding] = useState(false);
    const [requestedTemperature, setRequestedTemperature] = useState<number>(-99999);
    const temperatureClient = new TemperatureClient(API_URL);

    useEffect(() => {
        setStatus(props.status);
        if (!isSliding) {
            setRequestedTemperature(props.status.requested_temperature);
        }
    }, [props]);

    const sendRequestedTemperature = (value: number) => {
        if (requestedTemperature === status.requested_temperature) return;
        temperatureClient.sendTemperature(value);
    }

    const celsiusToFahrenheit = (celsius: number | undefined): number => {
        if (celsius == undefined) { return -9999; }

        return (celsius * 9/5) + 32;
    }


    return (
        <List subheader={
            <ListSubheader sx={{textShadow:'1px 1px #7E4EFFFF', color: "#ea63ff", fontSize:'1.2em'}}>
                Water Temperature
            </ListSubheader>
        }>
            <ListItem>
                <ListItemIcon >
                    <ThermostatIcon fontSize={"large"} />
                </ListItemIcon>
                <ListItemText>
                    Input Temperature
                    {<Chip
                        sx={{ml: 1, float: 'right'}}
                        label={`${status?.input_temperature.toFixed(2)}°C / ${celsiusToFahrenheit(status?.input_temperature).toFixed(2)}°F`}
                        color={"info"}
                        size={"medium"}
                    />}
                </ListItemText>
            </ListItem>
            <Divider/>
            <ListItem>
                <ListItemIcon >
                    <ThermostatIcon fontSize={"large"} />
                </ListItemIcon>
                <ListItemText>
                    Output Temperature
                    {<Chip
                        sx={{ml: 1, float: 'right'}}
                        label={`${status?.output_temperature.toFixed(2)}°C / ${celsiusToFahrenheit(status?.output_temperature).toFixed(2)}°F`}
                        color={"info"}
                        size={"medium"}
                    />}
                </ListItemText>
            </ListItem>
            <Divider />
            <ListItem>
                <ListItemIcon >
                    <ThermostatIcon fontSize={"large"} />
                </ListItemIcon>
                <ListItemText>
                    Wanted Temperature
                    {<Chip
                        sx={{ml: 1, float: 'right'}}
                        label={`${status?.requested_temperature.toFixed(2)}°C / ${celsiusToFahrenheit(status?.requested_temperature).toFixed(2)}°F`}
                        color={"default"}
                        size={"medium"}
                    />}
                </ListItemText>
            </ListItem>
            <ListItem>
                <Slider
                    min={0}
                    max={45}
                    marks={marks}
                    value={requestedTemperature}
                    step={0.5}
                    onChange={(_event, value) => {
                        setRequestedTemperature(value as number);
                        setIsSliding(true);
                    }}
                    onChangeCommitted={(_event) => {
                        sendRequestedTemperature(requestedTemperature);
                        setIsSliding(false);
                    }}
                />
            </ListItem>
            <Divider/>
            <ListItem>
                <ListItemIcon >
                    <LocalFireDepartmentIcon fontSize={"large"} />
                </ListItemIcon>
                <ListItemText
                >
                    Water Heater Status
                    {props.status.heater_status  ?
                        <Chip
                            sx={{ml: 1, float: 'right'}}

                            label="On"
                            color={"info"}
                            icon={<BoltIcon />}
                            size={"medium"}
                        />
                        :
                        <Chip
                            sx={{ml: 1, float: 'right'}}
                            label="Off"
                            color={"default"}
                            icon={<OfflineBoltIcon />}
                            size={"medium"}
                        />
                    }
                </ListItemText>
            </ListItem>
        </List>
    );
}

export default WaterHeater;