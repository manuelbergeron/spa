import {Status} from "../api/Status.ts";
import React  from "react";
import {Chip, Divider, List, ListItem, ListItemIcon, ListItemText, ListSubheader} from "@mui/material";
import AirIcon from '@mui/icons-material/Air';
import OnOffComponent from "./OnOffComponent.tsx";
import {API_URL} from "../constant.ts";
import LightClient from "../api/LightClient.ts";
import LightbulbIcon from '@mui/icons-material/Lightbulb';
import BlowerClient from "../api/BlowerClient.ts";
import ScheduleIcon from '@mui/icons-material/Schedule';
import BlockIcon from "@mui/icons-material/Block";

interface OtherSystemsProps {
    status: Status
}

const OtherSystems : React.FC<OtherSystemsProps> = (props: OtherSystemsProps) => {
    const lightClient = new LightClient(API_URL);
    const blowerClient = new BlowerClient(API_URL);

    return (
        <List subheader={
            <ListSubheader sx={{textShadow:'1px 1px #7E4EFFFF', color: "#ea63ff", fontSize:'1.2em'}}>
                Other Systems
            </ListSubheader>
        }>
            <ListItem>
                <ListItemIcon >
                    <ScheduleIcon fontSize={"large"} />
                </ListItemIcon>
               <ListItemText
               >
                   In Schedule
                   {props.status.is_in_schedule ?
                       <Chip
                           sx={{ml: 1, float: 'right'}}
                           label="Yes"
                           color={"info"}
                           size={"medium"}
                           icon={<ScheduleIcon/>}
                       />
                       :
                       <Chip
                           sx={{ml: 1, float: 'right'}}
                           label="No"
                           icon={<BlockIcon/>}
                           color={"default"}
                           size={"medium"}
                       />}
               </ListItemText>
            </ListItem>
            <Divider/>
            <ListItem>
                <ListItemIcon >
                    <AirIcon  fontSize={"large"}/>
                </ListItemIcon>
                <ListItemText>
                    <OnOffComponent
                        value={props.status.blower_status}
                        requestedValue={props.status.requested_blower_status}
                        label={"Bubbles"}
                        key={"Bubbles"}
                        onValueChange={(newValue:boolean) => {
                            blowerClient.sendRequestedStatus(newValue);
                        }}
                    />
                </ListItemText>
            </ListItem>
            <Divider/>
            <ListItem>
                <ListItemIcon >
                    <LightbulbIcon  fontSize={"large"} />
                </ListItemIcon>
                <ListItemText>
                    <OnOffComponent
                        value={props.status.light_status}
                        requestedValue={props.status.requested_light_status}
                        label={"Light"}
                        key={"Light"}
                        onValueChange={(newValue:boolean) => {
                            lightClient.sendRequestedStatus(newValue);
                        }}
                    />
                </ListItemText>

            </ListItem>
        </List>
     );
};

export default OtherSystems;