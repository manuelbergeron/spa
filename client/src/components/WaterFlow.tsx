import React, {useEffect, useState} from "react";
import {API_URL} from "../constant.ts";
import {CirculationPumpClient} from "../api/CirculationPumpClient.ts";
import {Chip, Divider, List, ListItem, ListItemIcon, ListItemText, ListSubheader} from "@mui/material";
import ChangeCircleIcon from '@mui/icons-material/ChangeCircle';
import WaterDropIcon from "@mui/icons-material/WaterDrop";
import FormatColorResetIcon from "@mui/icons-material/FormatColorReset";
import {Status} from "../api/Status.ts";
import OnOffComponent from "./OnOffComponent.tsx";
import PumpSpeedComponent from "./PumpSpeedComponent.tsx";
import {PumpSpeed} from "../api/PumpSpeed.ts";
import CleaningServicesIcon from '@mui/icons-material/CleaningServices';
import BlockIcon from '@mui/icons-material/Block';
import CleaningPumpClient from "../api/CleaningPumpClient.ts";
import JetPumpClient from "../api/JetPumpClient.ts";
interface WaterFlowProps {
    status: Status
}

const WaterFlow : React.FC<WaterFlowProps> = (props: WaterFlowProps) => {
    const [status, setStatus] = useState<Status>(props.status);
    const circulationPumpClient = new CirculationPumpClient(API_URL);
    const cleaningPumpClient = new CleaningPumpClient(API_URL);
    const jetPumpClient = new JetPumpClient(API_URL);

    const sendRequestedCirculationPump = (pumpStatus: boolean) => {
        circulationPumpClient.sendRequestedStatus(pumpStatus);
    }

    const sendRequestedCleaningPumpSpeed = (pumpSpeed: PumpSpeed) => {
        cleaningPumpClient.sendRequestedStatus(pumpSpeed);
    }

    const sendRquestedJetPumpStatus = (jetPumpStatus: boolean) => {
        jetPumpClient.sendRequestedStatus(jetPumpStatus);
    }

    useEffect(() => {
        setStatus(props.status);
    }, [props]);

    return (
        <List subheader={
            <ListSubheader sx={{textShadow:'1px 1px #7E4EFFFF', color: "#ea63ff", fontSize:'1.2em'}}>
                Water Flow
            </ListSubheader>
        }>
            <ListItem>
                <ListItemIcon>
                    <WaterDropIcon fontSize={"large"} />
                </ListItemIcon>
                <ListItemText
                >
                    Water Flow
                    {status.flow_status ?
                        <Chip
                            sx={{ml: 1, float: 'right'}}
                            label="Yes"
                            color={"info"}
                            size={"medium"}
                            icon={ <WaterDropIcon  aria-label={"Positive flow"} />}
                        />
                        :
                        <Chip
                            sx={{ml: 1, float: 'right'}}
                            label="No"
                            color={"default"}
                            size={"medium"}
                            icon={<FormatColorResetIcon/>}
                        />}
                </ListItemText>
            </ListItem>
            <Divider/>
            <ListItem>
                <ListItemIcon>
                    <CleaningServicesIcon fontSize={"large"} />
                </ListItemIcon>
                <ListItemText
                >
                    Is Cleaning
                    {status.is_cleaning ?
                        <Chip
                            sx={{ml: 1, float: 'right'}}
                            label="Yes"
                            color={"info"}
                            size={"medium"}
                            icon={ <CleaningServicesIcon  aria-label={"Positive flow"} />}
                        />
                        :
                        <Chip
                            sx={{ml: 1, float: 'right'}}
                            label="No"
                            color={"default"}
                            size={"medium"}
                            icon={<BlockIcon/>}
                        />}
                </ListItemText>
            </ListItem>
            <Divider/>
            <ListItem>
                <ListItemIcon >
                    <ChangeCircleIcon fontSize={"large"} />
                </ListItemIcon>
                <ListItemText>
                    <OnOffComponent
                        value={status.circulation_pump_status}
                        requestedValue={status.requested_circulation_pump_status}
                        label={"Flow Pump"}
                        key={"Flow Pump"}
                        onValueChange={(newValue:boolean) => {
                            sendRequestedCirculationPump(newValue);
                        }}
                    />
                </ListItemText>
            </ListItem>
            <Divider/>
            <ListItem>
                <ListItemIcon >
                    <ChangeCircleIcon fontSize={"large"} />
                </ListItemIcon>
                <ListItemText>
                    <PumpSpeedComponent
                        value={status.cleaning_pump_speed}
                        requestedValue={status.requested_cleaning_pump_speed}
                        label={"Cleaning Pump"}
                        key={"Cleaning Pump"}
                        onValueChange={(newValue:PumpSpeed) => {
                            sendRequestedCleaningPumpSpeed(newValue);
                        }}
                    />
                </ListItemText>
            </ListItem>
            <Divider/>
            <ListItem>
                <ListItemIcon >
                    <ChangeCircleIcon fontSize={"large"} />
                </ListItemIcon>
                <ListItemText>
                    <OnOffComponent
                        value={status.jet_pump_status}
                        requestedValue={status.requested_jet_pump_status }
                        label={"Jet Pump"}
                        key={"Jet Pump"}
                        onValueChange={(newValue:boolean) => {
                            sendRquestedJetPumpStatus(newValue);
                        }}
                    />
                </ListItemText>
            </ListItem>
        </List>


    );
}

export  default WaterFlow;