import {Grid  } from "@mui/material";
import { Paper} from "@mui/material";
import React, {useContext} from "react";
import StatusContext from "../contexts/StatusContext.tsx";
import WaterHeater from "./WaterHeater.tsx";
import WaterFlow from "./WaterFlow.tsx";
import OtherSystems from "./OtherSystems.tsx";
const SystemStatus : React.FC = () => {
    const { state:status  } = useContext(StatusContext);

    return (
        <Grid container spacing={1} sx={{mt:4}} >

            <Grid item xs={12} sx={{mb: 1}}>
                <Paper elevation={2} sx={{p:2}}  >
                    <WaterFlow status={status} />
                </Paper>
            </Grid>
            <Grid item xs={12} sx={{mb: 1}}>
                <Paper elevation={2} sx={{p:2}}  >
                    <WaterHeater status={status} />
                </Paper>
            </Grid>
            <Grid item xs={12}>
                <Paper elevation={2} sx={{p:2}}  >
                    <OtherSystems status={status} />
                </Paper>
            </Grid>
        </Grid>
    )
}

export default SystemStatus;