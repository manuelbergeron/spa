import React, {createContext, ReactNode, useReducer} from "react";
import {Status} from "../api/Status.ts";
import {PumpSpeed} from "../api/PumpSpeed.ts";


const initialStatusState: Status = {
    blower_status: false,
    circulation_pump_status: true,
    cleaning_pump_speed: PumpSpeed.OFF,
    flow_status:false,
    heater_status: false,
    input_temperature: 0,
    is_cleaning: false,
    is_in_schedule: true,
    jet_pump_status: false,
    light_status: false,
    output_temperature: 0,
    requested_blower_status: false,
    requested_circulation_pump_status: false,
    requested_cleaning_pump_speed: PumpSpeed.OFF,
    requested_jet_pump_status: false,
    requested_light_status:false,
    requested_temperature: 0
};

type StatusContextType = {
    state: Status,
    dispatch: React.Dispatch<StatusAction>
};

const StatusContext = createContext<StatusContextType>({
        state: initialStatusState,
        dispatch: () =>  {
            console.log("DISPATCH NOT IMPLEMENTED?");
        }
    }
);

/// REDUCER
type StatusAction = | {
    type: 'SET_STATUS_FROM_API';
    payload: Status;
};

const systemStatusReducer = (state: Status, action:  StatusAction): Status => {
    switch (action.type) {
        case 'SET_STATUS_FROM_API':
            state = action.payload;
            return  {
                ...state
            };
        default:
            return state;
    }
};

export const StatusProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
    const [state, dispatch] = useReducer(
        systemStatusReducer,
        initialStatusState
    );

    const systemStatusValue: StatusContextType = {
        state,
        dispatch,
    }

    return  <StatusContext.Provider value={systemStatusValue}>{children}</StatusContext.Provider>;
};

export default StatusContext;
