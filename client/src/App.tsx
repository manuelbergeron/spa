import './App.css';
import {Container, ThemeProvider} from "@mui/material";
import {useTheme} from '@mui/material/styles';
import {useSystemStatusClient} from "./api/StatusClient.ts";
import SystemStatus from "./components/SystemStatus.tsx";
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import AppBar from '@mui/material/AppBar';
import icon from './assets/icon.png';
import { Avatar } from '@mui/material';
const App = () => {
    const theme = useTheme();
    useSystemStatusClient();

    return (
        <>
            <ThemeProvider theme={theme}>
                <Container disableGutters>
                    <AppBar position="fixed" sx={{ zIndex: (theme) => theme.zIndex.drawer + 1, backgroundColor:"skyblue" }}>
                        <Toolbar>
                            <Avatar src={icon} sx={{ width: 42, height: 42, mr: 1}} alt={"icon"} />
                            <Typography variant="h6" noWrap color={"#ea63ff"} sx={{textShadow:'1px 1px #7E4EFFFF'}}>
                                My Little Spa!
                            </Typography>
                        </Toolbar>
                    </AppBar>
                    <SystemStatus />


                </Container>
            </ThemeProvider>
        </>
    )
}

export default App;
